print('Hello World')


def format_date(day, month, year):
    # Задача №1
    months = [
        'января', 'февраля', 'марта', 'апреля',
        'мая', 'июня', 'июля', 'августа',
        'сентября', 'октября', 'ноября', 'декабря'
    ]

    return f'{day} {months[month - 1]} {year} года'


def count_names(name_tuple):
    # Задача №2
    name_count = {}
    for name in name_tuple:
        if name in name_count:
            name_count[name] += 1
        else:
            name_count[name] = 1
    return name_count


def format_person_info(person_dict):
    # Задача №3
    # Форматирование строки в зависимости от наличия данных
    last_name = person_dict.get('last_name', '')
    first_name = person_dict.get('first_name', '')
    middle_name = person_dict.get('middle_name', '')

    if not last_name and not first_name:
        return 'Нет данных'

    # Если отсутствует фамилия, выводим только имя и отчество
    if not last_name and first_name:
        return f'{first_name} {middle_name}' if middle_name else first_name

    # Если отсутствует отчество, выводим фамилию и имя (при наличии)
    if last_name and not middle_name:
        return f'{last_name} {first_name}' if first_name else last_name

    # Если отсутствует имя, выводится только фамилия, отчество без имени выводить не надо
    if last_name and not first_name:
        return last_name

    # В противном случае выводим Фамилию Имя Отчество
    return f'{last_name} {first_name} {middle_name}'


def is_prime(number):
    # Задача №4
    if number < 2:
        return False
    for i in range(2, int(number ** 0.5) + 1):
        if number % i == 0:
            return False
    return True


def unique_numbers(*args):
    # Задача №5
    unique_nums = set()

    for arg in args:
        if isinstance(arg, (int, float)):
            unique_nums.add(arg)

    return sorted(list(unique_nums))

