from django.urls import path
from core.views import Infopage, Katalog, Create_e, Info_event, Create_p, Create_v, Create_c
from . import views


urlpatterns = [
    path('', Infopage.as_view(), name='info'),
    path("katalog/", Katalog.as_view(), name='katalog'),
    path("create/", Create_e.as_view(), name='create_e'),
    path("create_p/", Create_p.as_view(), name='create_p'),
    path("create_v/", Create_v.as_view(), name='create_v'),
    path("create_c/", Create_c.as_view(), name='create_c'),

    path("event/<id>/", Info_event.as_view(), name='i_event'),
    path('delete_event/<int:id>/', views.delete_event, name='delete_event'),
    path('delete_participant/<int:event_id>/<int:participant_id>/', views.delete_participant, name='delete_participant'),
]
