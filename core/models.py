from django.db import models


# Название проекта: Планировщик мероприятий

# Модель для хранения информации о местах проведения мероприятий
class Venue(models.Model):
    name_v = models.CharField('Название места', max_length=200)
    address = models.CharField('Адрес', max_length=200)
    capacity = models.PositiveIntegerField('Вместимость')

    def __str__(self):
        return self.name_v


# Модель для хранения информации о категориях мероприятий
class Category(models.Model):
    name_c = models.CharField('Категория', max_length=200)
    description = models.TextField('Описание')

    def __str__(self):
        return self.name_c


# Модель для хранения информации о участниках мероприятий
class Participant(models.Model):
    first_name = models.CharField('Имя', max_length=50)
    last_name = models.CharField('Фамилия', max_length=50)
    email = models.EmailField('e-mail')
    phone = models.CharField('телефон', max_length=15)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


# Модель для хранения информации о мероприятиях
class Event(models.Model):
    name_e = models.CharField('Название', max_length=200)
    description = models.TextField('Описание')
    start_datetime = models.DateTimeField('дата начала')
    end_datetime = models.DateTimeField('дата окончания')
    venue = models.ForeignKey(Venue, verbose_name='Место', on_delete=models.CASCADE, related_name='ev_venue')
    category = models.ForeignKey(Category, verbose_name='Категория', on_delete=models.SET_NULL, null=True,
                                 related_name='ev_category')
    participants = models.ManyToManyField(Participant, verbose_name='Участники', related_name='ev_participant')

    def __str__(self):
        return self.name_e
