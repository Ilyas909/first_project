from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from django.views.generic import ListView, DetailView, FormView, CreateView
from django.db.models import Q
from core import models, forms
from django.urls import reverse_lazy


class Infopage(ListView):
    template_name = 'core/index.html'
    extra_context = {'title': 'Мероприятия'}
    context_object_name = 'events'
    def get_queryset(self):
        queryset = models.Event.objects.all()
        sort_param = self.request.GET.get('sort')
        if sort_param=='name':
            queryset = queryset.order_by('name_e')
        elif sort_param=='start_datetime':
            queryset = queryset.order_by('start_datetime')

        return queryset


class Info_event(DetailView):
    template_name = 'core/event.html'
    model = models.Event
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.object.name_e

        search_query = self.request.GET.get('search')
        if search_query:
            participants = self.object.participants.filter(
                Q(first_name__icontains=search_query) |
                Q(last_name__icontains=search_query)
            )
            context['participants'] = participants
        else:
            context['participants'] = self.object.participants.all()

        return context


def delete_event(request, id):
    event = get_object_or_404(models.Event, id=id)

    if request.method == 'POST':
        event.delete()
        return redirect('info')


def delete_participant(request, event_id, participant_id):
    event = get_object_or_404(models.Event, id=event_id)
    participant = get_object_or_404(models.Participant, id=participant_id)

    if request.method == 'POST':
        # Удаляем участника из мероприятия
        event.participants.remove(participant)
        return redirect('i_event', id=event.id)


class Katalog(View):
        template_name = 'core/katalog.html'
        def get(self, request, *args, **kwargs):
            venues = models.Venue.objects.all()
            venue_events = {}

            for venue in venues:
                # Получаем все мероприятия, связанные с данным местом
                events = models.Event.objects.filter(venue=venue)
                # Собираем даты мероприятий для данного места
                event_dates = [f"{event.start_datetime.date()} - {event.end_datetime.date()}" for event in events]
                venue_events[venue] = event_dates

            context = {'venues': venues, 'venue_events': venue_events, 'title': 'Каталог'}
            return render(request, self.template_name, context)

class Create_e(FormView):
    template_name = 'core/create.html'
    form_class = forms.Events
    success_url = reverse_lazy('create_e')

    def form_valid(self, form):
        form.save()
        return redirect(self.get_success_url())


class Create_p(CreateView):
    model = models.Participant
    fields = ('first_name', 'last_name', 'email', 'phone')
    template_name = 'core/create_p.html'
    success_url = reverse_lazy('create_p')

    def form_valid(self, form):
        # Проверка существования записи с такими значениями полей
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        email = form.cleaned_data['email']
        phone = form.cleaned_data['phone']

        existing_participant = models.Participant.objects.filter(
            first_name=first_name,
            last_name=last_name,
            email=email,
            phone=phone
        ).exists()

        # Если запись существует, не добавляем новую, а перенаправляем пользователя
        if existing_participant:
            messages.error(self.request, 'Запись с такими данными уже существует.')
            return self.form_invalid(form)

        return super().form_valid(form)

class Create_v(CreateView):
    model = models.Venue
    fields = ('name_v', 'address', 'capacity')
    template_name = 'core/create_v.html'
    success_url = reverse_lazy('create_v')

    def form_valid(self, form):
        # Проверка существования записи с такими значениями полей
        name_v = form.cleaned_data['name_v']
        address = form.cleaned_data['address']
        capacity = form.cleaned_data['capacity']

        existing_v = models.Venue.objects.filter(
            name_v=name_v,
            address=address,
            capacity=capacity
        ).exists()

        # Если запись существует, не добавляем новую, а перенаправляем пользователя
        if existing_v:
            messages.error(self.request, 'Запись с такими данными уже существует.')
            return self.form_invalid(form)

        return super().form_valid(form)

class Create_c(CreateView):
    model = models.Category
    fields = ('name_c', 'description')
    template_name = 'core/create_c.html'
    success_url = reverse_lazy('create_c')

    def form_valid(self, form):
        # Проверка существования записи с такими значениями полей
        name_c = form.cleaned_data['name_c']
        description = form.cleaned_data['description']

        existing_participant = models.Category.objects.filter(
            name_c=name_c,
            description=description
        ).exists()

        # Если запись существует, не добавляем новую, а перенаправляем пользователя
        if existing_participant:
            messages.error(self.request, 'Запись с такими данными уже существует.')
            return self.form_invalid(form)

        return super().form_valid(form)