import datetime
from django import forms
from core.models import Event


class Events(forms.ModelForm):
    class Meta:
        model = Event
        fields = ('name_e', 'description', 'start_datetime', 'end_datetime', 'venue', 'category', 'participants')

    def clean_time(self):
        cleaned_data = super().clean()
        start_datetime = cleaned_data.get('start_datetime')
        end_datetime = cleaned_data.get('end_datetime')

        if end_datetime and start_datetime:
            if end_datetime < start_datetime or start_datetime < datetime.datetime.now():
                raise forms.ValidationError('Дата указана неправильно')

        return cleaned_data

    def clean(self):
        cleaned_data = super().clean()
        start_datetime = cleaned_data.get('start_datetime')
        end_datetime = cleaned_data.get('end_datetime')
        venue = cleaned_data.get('venue')

        if start_datetime and end_datetime and venue:
            # Проверка, что место свободно в указанные дни
            conflicting_events = Event.objects.filter(
                venue=venue,
                start_datetime__lt=end_datetime,
                end_datetime__gt=start_datetime
            )

            if conflicting_events.exists():
                raise forms.ValidationError('Место занято в указанный период времени.')

        return cleaned_data

